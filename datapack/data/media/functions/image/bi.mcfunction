# Load byte
$execute store result score byte media_math run data get storage media:images $(name).bytes[$(i)] 1

# Render pixel
execute if score byte media_math matches 0 run data modify storage media:render text append value '{"text":"⬛","color":"#FFFFFF"}'
execute if score byte media_math matches 1 run data modify storage media:render text append value '{"text":"⬛","color":"#000000"}'

# Inc i
scoreboard players add i media_math 1
execute store result storage media:render i int 1 run scoreboard players get i media_math

# If i is equal to length return, otherwise loop
execute unless score i media_math = frame_length media_math run function media:image/bi with storage media:render