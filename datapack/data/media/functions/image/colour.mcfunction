# Load bytes
$data modify storage media:render red_byte set from storage media:images $(name).bytes[$(r)]
$data modify storage media:render green_byte set from storage media:images $(name).bytes[$(g)]
$data modify storage media:render blue_byte set from storage media:images $(name).bytes[$(b)]

# Render pixel
function media:pixel/colour with storage media:render

# Inc r, g, and b
scoreboard players add r media_math 3
execute store result storage media:render r int 1 run scoreboard players get r media_math
scoreboard players add g media_math 3
execute store result storage media:render g int 1 run scoreboard players get g media_math
scoreboard players add b media_math 3
execute store result storage media:render b int 1 run scoreboard players get b media_math

# If r is equal to length return, otherwise loop
execute unless score r media_math = frame_length media_math run function media:image/colour with storage media:render