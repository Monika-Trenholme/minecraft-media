# Load byte
$data modify storage media:render byte set from storage media:images $(name).bytes[$(i)]

# Render pixel
function media:pixel/gray with storage media:render

# Inc i
scoreboard players add i media_math 1
execute store result storage media:render i int 1 run scoreboard players get i media_math

# If i is equal to length return, otherwise loop
execute unless score i media_math = frame_length media_math run function media:image/gray with storage media:render