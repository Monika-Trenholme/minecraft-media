# Get width and height to calculate overall length
$execute store result score frame_width media_math run data get storage media:images $(name).width 1 
$execute store result score frame_height media_math run data get storage media:images $(name).height 1
scoreboard players operation frame_length media_math = frame_width media_math
scoreboard players operation frame_length media_math *= frame_height media_math

# Setup data for pixel loading
$execute store result score frame_format media_math run data get storage media:images $(name).format 1
$data modify storage media:render name set value "$(name)"
data modify storage media:render text set value []
scoreboard players set i media_math 0
data modify storage media:render i set value 0
scoreboard players set r media_math 0
data modify storage media:render r set value 0
scoreboard players set g media_math 1
data modify storage media:render g set value 1
scoreboard players set b media_math 2
data modify storage media:render b set value 2

# If format is 0 run black and white function
execute if score frame_format media_math matches 0 run function media:image/bi with storage media:render
# If format is 1 run gray function
execute if score frame_format media_math matches 1 run function media:image/gray with storage media:render
# If format is 2 run colour function
execute if score frame_format media_math matches 2 run scoreboard players operation frame_length media_math *= rgb media_math
execute if score frame_format media_math matches 2 run function media:image/colour with storage media:render

# Calculate transforms
$data modify storage media:render scale set value $(scale)
execute store result storage media:render dx float 0.00003 run data get storage media:render scale 1000
execute store result storage media:render dy float 0.000075 run data get storage media:render scale 1000
execute store result storage media:render xscale float 0.0012 run data get storage media:render scale 1000

# Calculate line width and create/update text displays
execute store result storage media:render width int 8 run scoreboard players get frame_width media_math
$data modify storage media:render tag set value "$(tag)"
$execute if entity @e[type=minecraft:text_display,tag=$(tag)] run function media:display/update with storage media:render
$execute unless entity @e[type=minecraft:text_display,tag=$(tag)] run function media:display/create with storage media:render

# Cleanup
data remove storage media:render text