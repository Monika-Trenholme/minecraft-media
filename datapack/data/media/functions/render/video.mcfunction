execute if score playing_video media_math matches 1 run tellraw @s {"text":"Can't play more than one video at once","color":"red"}
execute if score playing_video media_math matches 1 run return 1

$data modify storage media:playing tag set value "$(tag)"
$data modify storage media:playing name set value "$(name)"
$data modify storage media:playing scale set value $(scale)
$execute store result score video_length media_math run data get storage media:videos $(name).len

execute if score len media_math matches -2147483648..0 run tellraw @s {"text":"Vedio length is set less than or equal to 0, not playing","color":"red"}
execute if score len media_math matches -2147483648..0 run return 1

scoreboard players set playing_video media_math 1

$data modify storage media:images video_frame.format set from storage media:videos $(name).format
$data modify storage media:images video_frame.width set from storage media:videos $(name).width
$data modify storage media:images video_frame.height set from storage media:videos $(name).height

scoreboard players set frame media_math 0
data modify storage media:playing frame set value 0

function media:video/loop