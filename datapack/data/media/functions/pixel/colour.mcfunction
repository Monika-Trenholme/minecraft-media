$data modify storage media:render red_hex set from storage media:root comp.$(red_byte)
$data modify storage media:render green_hex set from storage media:root comp.$(green_byte)
$data modify storage media:render blue_hex set from storage media:root comp.$(blue_byte)
function media:pixel/colour2 with storage media:render