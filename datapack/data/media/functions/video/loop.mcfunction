# Render frame
function media:video/render_frame with storage media:playing

# Inc frame
scoreboard players add frame media_math 1
execute store result storage media:playing frame int 1 run scoreboard players get frame media_math

# If frame is equal to video length set playing bool to 0
execute if score frame media_math = video_length media_math run scoreboard players set playing_video media_math 0

# If still playing reschedule
execute if score playing_video media_math matches 1 run schedule function media:video/loop 2t