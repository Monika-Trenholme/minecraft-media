# Load frame
$data modify storage media:images video_frame.bytes set from storage media:videos $(name).frames[$(frame)]

# Render frame
$function media:render/image {name:"video_frame",tag:"$(tag)",scale:$(scale)}