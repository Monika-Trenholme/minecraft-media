use nbt::{List, NBTString, NBT};
use std::{
    collections::HashMap,
    env::args,
    error::Error,
    fmt,
    fs::{self, File},
    io::Read,
    mem,
    path::PathBuf,
};

const PBM: u8 = 4;
const PGM: u8 = 5;
const PPM: u8 = 6;

#[derive(PartialEq, Eq)]
struct ImageSize(i16, i16);

enum NetPBM {
    PBM(ImageSize, Vec<i8>),
    PGM(ImageSize, Vec<i8>),
    PPM(ImageSize, Vec<i8>),
}

#[derive(Debug)]
enum ImageError {
    Header,
    InvalidFormatNumber(u8),
    Depth,
    ASCII,
    PAM,
    IO(std::io::Error),
}

impl fmt::Display for ImageError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Header => write!(f, "invalid header data, note: this program only supports max width/height of i16 limit or {}", i16::MAX),
            Self::InvalidFormatNumber(n) => write!(f, "invalid format number {n}"),
            Self::Depth => write!(f, "this program only supports colour depth of 255"),
            Self::ASCII => write!(f, "this program doesn't have NetPBM ascii support sorry :<"),
            Self::PAM => write!(f, "this program doesn't have PAM support sorry :<"),
            Self::IO(e) => write!(f, "{e}"),
        }
    }
}

impl From<std::io::Error> for ImageError {
    fn from(value: std::io::Error) -> Self {
        Self::IO(value)
    }
}

impl Error for ImageError {}

impl NetPBM {
    fn deserialise(mut file: File) -> Result<Self, ImageError> {
        let mut buffer = [0_u8; 6]; // Just enough to hold i16::MAX and terminator
        file.read_exact(&mut buffer[0..1])?;
        if buffer[0] != b'P' {
            return Err(ImageError::Header);
        }
        file.read_exact(&mut buffer[0..1])?;
        if !matches!(buffer[0], b'0'..=b'9') {
            return Err(ImageError::Header);
        }
        let format_number = match buffer[0] - b'0' {
            1..=3 => return Err(ImageError::ASCII),
            n @ 4..=6 => n,
            7 => return Err(ImageError::PAM),
            n => return Err(ImageError::InvalidFormatNumber(n)),
        };
        file.read_exact(&mut buffer[0..1])?;
        if buffer[0] != b'\n' {
            return Err(ImageError::Header);
        }
        let mut i = 0;
        while i < 6 {
            file.read_exact(&mut buffer[i..i + 1])?;
            if buffer[i] == b' ' {
                break;
            }
            i += 1;
        }
        if buffer[i.clamp(0, 5)] != b' ' {
            return Err(ImageError::Header);
        }
        let s =
            String::from_utf8(Vec::from(&buffer[0..i] as &[u8])).map_err(|_| ImageError::Header)?;
        let u = s.parse::<u16>().map_err(|_| ImageError::Header)?;
        let width: i16 = u.try_into().map_err(|_| ImageError::Header)?;
        let mut i = 0;
        while i < 6 {
            file.read_exact(&mut buffer[i..i + 1])?;
            if buffer[i] == b'\n' {
                break;
            }
            i += 1;
        }
        if buffer[i.clamp(0, 5)] != b'\n' {
            return Err(ImageError::Header);
        }
        let s =
            String::from_utf8(Vec::from(&buffer[0..i] as &[u8])).map_err(|_| ImageError::Header)?;
        let u = s.parse::<u16>().map_err(|_| ImageError::Header)?;
        let height: i16 = u.try_into().map_err(|_| ImageError::Header)?;
        if format_number != PBM {
            file.read_exact(&mut buffer[0..3])?;
            if &buffer[0..3] != &[b'2', b'5', b'5'] {
                return Err(ImageError::Depth);
            }
            file.read_exact(&mut buffer[0..1])?;
            if buffer[0] != b'\n' {
                return Err(ImageError::Header);
            }
        }
        match format_number {
            PBM => {
                let width_rem_bits = width as usize % 8;
                let width_bytes = width as usize / 8 + width_rem_bits.min(1);
                let mut bytes = vec![0_u8; width_bytes * height as usize];
                file.read_exact(&mut bytes)?;
                let mut iter = bytes.into_iter();
                let mut bytes = Vec::new();
                while let Some(mut byte) = iter.next() {
                    let bits = if iter.len() % width_bytes == 0 && width_rem_bits != 0 {
                        width_rem_bits
                    } else {
                        8
                    };
                    for _ in 0..bits {
                        bytes.push((byte & 128 != 0) as i8);
                        byte <<= 1;
                    }
                }
                Ok(NetPBM::PBM(ImageSize(width, height), bytes))
            }
            PGM => {
                let mut bytes = vec![0_u8; width as usize * height as usize];
                file.read_exact(&mut bytes)?;
                let new_bytes = unsafe {
                    let len = bytes.len();
                    let capacity = bytes.capacity();
                    let ptr = bytes.as_mut_ptr();
                    mem::forget(bytes);
                    Vec::from_raw_parts(ptr as *mut i8, len, capacity)
                };
                Ok(NetPBM::PGM(ImageSize(width, height), new_bytes))
            }
            PPM => {
                let mut bytes = vec![0_u8; width as usize * height as usize * 3];
                file.read_exact(&mut bytes)?;
                let new_bytes = unsafe {
                    let len = bytes.len();
                    let capacity = bytes.capacity();
                    let ptr = bytes.as_mut_ptr();
                    mem::forget(bytes);
                    Vec::from_raw_parts(ptr as *mut i8, len, capacity)
                };
                Ok(NetPBM::PPM(ImageSize(width, height), new_bytes))
            }
            _ => unreachable!(),
        }
    }
}

enum MediaType {
    Image,
    Video,
}

fn main() {
    let mut args = args();
    args.next();
    let storage = args.next().expect("Missing storage file argument");
    let media_type = match args.next().expect("Missing media type argument").as_ref() {
        "image" => MediaType::Image,
        "video" => MediaType::Video,
        s => panic!("Unrecognised media type `{s}`"),
    };
    let name = args.next().expect("Missing name argument");
    let media_file = args.next().expect("Missing media file argument");
    let mut storage_file = File::open(&storage).expect("Error opening nbt file");
    let storage_nbt: HashMap<_, _> = NBT::deserialise(&mut storage_file)
        .expect("Error deserialising storage nbt")
        .try_into()
        .unwrap();

    let storage_nbt = match media_type {
        MediaType::Image => add_image(storage_nbt, name, media_file),
        MediaType::Video => add_video(storage_nbt, name, media_file),
    };

    let mut storage_file = File::create(&storage).expect("Error opening nbt file");

    storage_nbt
        .serialise(&mut storage_file)
        .expect("Error serialisng nbt");
}

fn add_image(mut storage: HashMap<NBTString, NBT>, name: String, image_file: String) -> NBT {
    let mut root: HashMap<_, _> = storage
        .remove(&"".try_into().unwrap())
        .unwrap()
        .try_into()
        .unwrap();
    let mut data: HashMap<_, _> = root
        .remove(&"data".try_into().unwrap())
        .unwrap()
        .try_into()
        .unwrap();
    let mut contents: HashMap<_, _> = data
        .remove(&"contents".try_into().unwrap())
        .unwrap()
        .try_into()
        .unwrap();
    let mut images: HashMap<_, _> = contents
        .remove(&"images".try_into().unwrap())
        .unwrap()
        .try_into()
        .unwrap();
    let file = File::open(&image_file).expect("Error opening image file");
    let image_data = NetPBM::deserialise(file).expect("Error deserialising image file");
    let (format, width, height, bytes) = match image_data {
        NetPBM::PBM(ImageSize(w, h), bytes) => (0_i8, w, h, bytes),
        NetPBM::PGM(ImageSize(w, h), bytes) => (1_i8, w, h, bytes),
        NetPBM::PPM(ImageSize(w, h), bytes) => (2_i8, w, h, bytes),
    };
    let image = NBT::from(HashMap::from([
        ("format".try_into().unwrap(), format.into()),
        ("width".try_into().unwrap(), width.into()),
        ("height".try_into().unwrap(), height.into()),
        ("bytes".try_into().unwrap(), bytes.try_into().unwrap()),
    ]));
    images.insert(name.as_str().try_into().unwrap(), image.into());
    contents.insert("images".try_into().unwrap(), images.into());
    data.insert("contents".try_into().unwrap(), contents.into());
    root.insert("data".try_into().unwrap(), data.into());
    storage.insert("".try_into().unwrap(), root.into());
    NBT::from(storage)
}

fn add_video(mut storage: HashMap<NBTString, NBT>, name: String, frame_folder: String) -> NBT {
    let mut root: HashMap<_, _> = storage
        .remove(&"".try_into().unwrap())
        .unwrap()
        .try_into()
        .unwrap();
    let mut data: HashMap<_, _> = root
        .remove(&"data".try_into().unwrap())
        .unwrap()
        .try_into()
        .unwrap();
    let mut contents: HashMap<_, _> = data
        .remove(&"contents".try_into().unwrap())
        .unwrap()
        .try_into()
        .unwrap();
    let mut videos: HashMap<_, _> = contents
        .remove(&"videos".try_into().unwrap())
        .unwrap()
        .try_into()
        .unwrap();
    let dir = fs::read_dir(&frame_folder).expect("Error reading frame dir");
    let mut paths = dir
        .map(|res| res.map(|entry| entry.path()))
        .collect::<std::io::Result<Vec<PathBuf>>>()
        .expect("Error iterating over frame files");
    paths.sort();
    let len = paths.len();
    let mut paths_iter = paths.into_iter();
    let initial_path = paths_iter.next().expect("Error frame dir empty");
    let initial_file = File::open(initial_path).expect("Error opening frame");
    let (initial_size, initial_bytes, initial_format) =
        match NetPBM::deserialise(initial_file).expect("Error deserialisng frame") {
            NetPBM::PBM(size, bytes) => (size, bytes, 0_i8),
            NetPBM::PGM(size, bytes) => (size, bytes, 1_i8),
            NetPBM::PPM(size, bytes) => (size, bytes, 2_i8),
        };
    let mut video_frames = List::new();
    video_frames
        .push(initial_bytes.try_into().unwrap())
        .unwrap();
    for path in paths_iter {
        let file = File::open(path).expect("Error opening frame");
        let (size, bytes, format) =
            match NetPBM::deserialise(file).expect("Error deserialisng frame") {
                NetPBM::PBM(size, bits) => (size, bits, 0_i8),
                NetPBM::PGM(size, bytes) => (size, bytes, 1_i8),
                NetPBM::PPM(size, bytes) => (size, bytes, 2_i8),
            };
        if format != initial_format {
            panic!("Error frames are not of a consistent format");
        }
        if size != initial_size {
            panic!("Error frames are not of a consistent size");
        }
        video_frames.push(bytes.try_into().unwrap()).unwrap();
    }
    let video = NBT::from(HashMap::from([
        ("frames".try_into().unwrap(), video_frames.into()),
        ("width".try_into().unwrap(), initial_size.0.into()),
        ("height".try_into().unwrap(), initial_size.1.into()),
        ("format".try_into().unwrap(), initial_format.into()),
        ("len".try_into().unwrap(), (len as i32).into()),
    ]));
    videos.insert(name.as_str().try_into().unwrap(), video.into());
    contents.insert("videos".try_into().unwrap(), videos.into());
    data.insert("contents".try_into().unwrap(), contents.into());
    root.insert("data".try_into().unwrap(), data.into());
    storage.insert("".try_into().unwrap(), root.into());
    NBT::from(storage)
}
