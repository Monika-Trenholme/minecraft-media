# Dynamic Imgae/Video Rendering for Minecraft
This is a dynamic image and video renderer for Minecraft.

It supports:
- NetPBM P4 black and white (.pbm)
- NetPBM P5 gray scale with depth 255 (.pgm)
- NetPBM P6 colour with depth 255 (.ppm)
- Video at 10 fps with frames encoded as above

The src/main.rs program can aide in loading image and video files into a minecraft world by editing a command_storage.dat file.

External programs used in the following tutorial:
- gzip/gunzip
- ffmpeg
- cargo/rustup
The provided terminal commands are for Linux but should work for Windows and MacOS too

## Setup in World
You want to create a storage file called media with the compounds images and videos, the below commands can do that.
```
/data modify storage media:images holder set value 0
/data modify storage media:videos holder set value 0
```
After you can get the .dat file from world_save/data/command_storage_media.dat, it will compressed as a gzip archive so you will ned to use gunzip to decompress it, you'll probably need to append .gz to the file name.

## Load Image into DAT File
First you need to convert your file into one of the supported file types, below are ffmpeg commands to convert the image files. Here an output scale of 16x16 is provided but you can change that to any reasonable numbers you want. 

black and white:
```
ffmpeg -i input_image -vf scale=16:16 output_image.pbm
```

gray:
```
ffmpeg -i input_image -vf scale=16:16 output_image.pgm
```

colour:
```
ffmpeg -i input_image -vf scale=16:16 output_image.ppm
```

After you have your output_image you can use cargo to build and run the program to insert it into the command_storage.dat file. Make sure you have setup the NBT correctly and have ungzipped the DAT file, otherwise the program will panic and error out. Here "image_name" is the name it will have in game, it can be any valid UTF-8 string but do avoid spaces for your own sanity, the name video_frame is used by the datapack to render videos so please don't use that exact name.

```
cargo run -r -- command_storage_media.dat image image_name output_image
```

## Load Video into DAT File
First you need to convert your video into frames of one of the supported file types, below are ffmpeg commands to convert the video files. Video rendering is *very* heavy, so the datapack only supports rendering one video at a time, and plays at 10 fps. Here an output scale of 16x16 is provided but you can change it to any reasonable numbers you want, I was able to render 32x24 gray scale pretty smoothly on my machine. For the "%04d" part you may want to increase to 5 or 6 if your video is especially long (over 15 minutes).

black and white:
```
mkdir frames
cd frames
ffmpeg -i ../*.mp4 -vf "fps=10, scale=16:16" %04d.pbm
```

gray:
```
mkdir frames
cd frames
ffmpeg -i ../*.mp4 -vf "fps=10, scale=32:24" %04d.pgm
```

colour:
```
mkdir frames
cd frames
ffmpeg -i ../*.mp4 -vf "fps=10, scale=32:24" %04d.ppm
```

After you have your output_video you can use cargo to build and run the program to insert it into the command_storage.dat file. Make sure you have setup the NBT correctly and have ungzipped the DAT file, otherwise the program will panic and error out. Here "video_name" is the name it will have in game, it can be any valid UTF-8 string but do avoid spaces for your own sanity. This process may take some time.

```
cargo run -r -- command_storage_media.dat video video_name frames
```

## Render Image and Video in Game
After loading your image and video files recompress your dat file with gzip and put the file back into world_save/data, and make sure you have the datapack loaded. The "tag" parameter gives the summoned text displays a custom tag which you can use to either render new content onto or destroy them. Keep in mind though that the "scale" parameter is a one time thing and won't update the scale of the display on subsequent invokations.

render image:
```
/function media:render/image {name:image_name,tag:custom_tag,scale:1f}
```

render video:
```
/function media:render/video {name:video_name,tag:custom_tag,scale:1f}
```

stop video:
```
/function media:video/stop
```

destroy text display:
```
/function media:display/destroy {tag:custom_tag}
```

## Important Note
Minecraft really doesn't like the amount of data needed to handle raw image and video and render it via strings, it is very easy to make the game crash due to using up too much memory. I myself made the game crash but inadvertently making the game stringify the the list of bytes in a video with the /data get command. Just be careful emkay?

Also minecraft will not save the text display contents on world save if it's too large.

## Dynamic Image/Video Rendering
Here I will give an overview of how to use this in other projects if you feel inclined.

### Dynamic Image Rendering
This is useful if you want to generate a one off image, or to make some sort of animation or interactive display.

media:images is an nbt compund where each key is a name used by the media:render/image command, the name video_frame is special as it is used by the datapack internally to render videos. Each value is a compund with the associated data for the image.

Each image has these properties
- format
- width
- height
- bytes

The format is a byte that can be either 0, 1, or 2
- 0 each pixel is 1 byte that is either 0 for white or 1 for black, other pixel values won't be rendered
- 1 each pixel is 1 byte for a gray scale value
- 2 each pixel is 3 bytes for red, green, and blue

The width and height are both ints that give the dimensions of the image

Bytes is a byte array that stores the bytes for the image

With all that here is you can make a black and white cross shape
```
/data modify storage media:image cross set value {format:0b,width:3,height:3,bytes:[B;0b,1b,0b,1b,1b,1b,0b,1b,0b]}
```

### Dynamic Video
This is useful for one off videos mostly. Keep in mind that the datapack only supports playing one video at a time at 10 fps, if you want to play multiple videos at a time or at a different fps, you will just have to load the frames as images yourself.

media:videos is a compound where each key is a name used by the media:render/video command. Each value is a compound with the associated data for the video.

Each video has these properties
- format
- width
- height
- len
- frames

Format, width, and height are the same as images above.

Len is the number of frames as an int.

Frames is a list of byte arrays that hold the bytes for the frames.

## Something I learnt about JSON generation
The way this works is by converting the pixels into text JSON like this:
```
'{"text":"⬛","color":"#FF8800"}'
```

Initially I concatenated these pixel strings naively with macros like this:
```
'$(display_text)$(pixel_string)'
```

But I noticed as it got through the pixels rendering each one got slower and slower. I now believe that due to UTF-8 Minecraft's macro substitution is O(log(n)) which can make string concatenation like this for large strings very slow.

The solution? Use the nbt parameter along with interpret on a list.

Have a list some where like storage hello:world texts, with contents like this
```
['{"text":"h"}','{"text":"i"}']
```

Now if you run this command
```
/tellraw @s {"nbt":"texts","storage":"hello:world",interpreter:true,separator:""}
```
Then it will resolve to the string "hi"

This method of string concatenation is much more optimal.